# webvpythonparaprofs

WebVPython para professores

## Tópicos

### Criaćão de objetos
* sphere
* arrow
* cylinder
* cone 
* points
* text e label
* curve
* ring
* pyramid
* helix
* ellipsoid
* vertex
* shapes and paths

#### Efeitos
* light
* texture
* extrusion
* compound
* 

### Animação 
* rotação
* translação
* rate
* trail

#### Práticas (estáticas e dinâmicas)
* Eixos coordenados
* Operações com vetores
* MRU e MRUV
* Queda Livre
* Movimento circular
* Colisões
* Lançamento oblíquo
* Pêndulo
* Plano inclinado
* Ótica
* Órbitas 
* Força de Coulomb

Escolher umas cinco para fechar o tutorial/curso

### Gráficos

### Operações matemáticas

### Eventos

### Widgets

* Componentes
* Instruções

## Referências

* https://www.glowscript.org/#/user/matterandinteractions/folder/matterandinteractions/
* https://www.glowscript.org/#/user/GlowScriptDemos/folder/matterandinteractions/program/MatterAndInteractions